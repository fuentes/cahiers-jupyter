---
jupyter:
  jupytext:
    formats: ipynb,md
    text_representation:
      extension: .md
      format_name: markdown
      format_version: '1.0'
      jupytext_version: 1.0.2
  kernelspec:
    display_name: Python 2
    language: python2
    name: python2
---

# Introduction à Python pour le calcul numérique 
## Introduction
Le but de TP est de vous apprendre à utiliser **Python** comme outil de prototypage rapide en calcul numérique :  
- faire du calcul matriciel, 
- utiliser des fonctions mathématiques, 
- afficher des résultats graphiques.

Pour ce projet, nous allons principalement utiliser des modules d'extension de **Python** qui
permettent de faire du calcul numérique, respectivement **Numpy**, **Scipy** et **Matplotlib**.


### Démarrage
Dans tout ce TP, nous allons travailler avec l'interprète **IPython** ou un bloc-note **Jupyter**. Pour **IPython**
vous pouvez taper des commandes de manière interactive ainsi que copier-coller du code depuis votre éditeur 
favori (vim, emacs) avec la «commande magique» `%cpaste`, que l'on termine avec `--`
ou alors executer un script avec la commande 

    run monscript.py
    
Dans l'interprète, vous pouvez avoir facilement la documentation sur une fonction en utilisant `help(fonction)` ou `?function`


### Import de modules
Pour manipuler des matrices denses en **Python**, on peut charger toutes les fonctions du module **Numpy** à l'aide de la commande `import`
# ```python
from numpy import *
a = zeros((4,4))
# ```
ou le charger avec un préfixe 
# ```python
import numpy as np
a = np.zeros((4,4))
# ```
ou encore importer que uniquement certaines fonctions, par exemples
# ```python
from numpy import ones, dot
from numpy.random import rand
a = rand(4,4)
b = ones(4)
c = dot(a,b)
# ```
l'avantage du second cas est de ne pas polluer l'espace de noms, mais oblige à manipuler chaque fonction avec un préfixe. Quant à la dernière méthode elle ne charge que les fonctions demandées ce qui rend l'importation plus rapide. Dans ce TP, on utilisera la premiere méthode,

```python2
from numpy import *
```

Dans **IPython** vous pouvez utiliser la commande magique %pylab, pour faire les imports de **Numppy**, **MatPlotLib** et **Scipy**


## Guide de survie en environnement matriciel
### Commandes basiques
Pour effectuer le TP, vous aurez besoin des commandes suivantes

| nom                        | usage                                    |
|----------------------------|------------------------------------------|
| `zeros((m,n,...))`         | alloue une matrice avec des 0            |
| `ones(((m,n,...))`         | alloue une matrice avec des 1            |
| `arange(deb,fin,pas)`      | crée un vecteur de `deb` à `fin-1`       |
| `linspace(a,b,n)`          | crée un vecteur de `n` valeurs `a` à `b` |
| `eye(n)`                   | renvoies la matrice unité d'ordre  ` n`  |
| `diag(v)`                  | crée une matrice avec `v` pour diagonale |
| `diag(m)`                  | extrait la diagonale de `m`              |
| `a.shape`, ` shape(a)`     | renvoies les dims de a                   |
| `a.T` ,` transpose(a)`     | transposée de a                          |
| `a.flatten()`                | aplatit une matrice en vecteur           |
| `vstack((a,b))`, `r_[a,b]` | concatène verticalement `a` et `b`       |
| `hstack((a,b))`, `c_[a,b]` | concatène horizontalement `a` et `b`     |
| `dot(a,b)`                 | produit matriciel classique              |


### Quelques remarques
- les indices des *tranches* commencent toujours à zéro et ne contiennent pas le dernier élément : 

```python2
a = zeros((4,3))
all(a[0:4,1]==a[:,1]) 
```

- le dernier élément d'une dimension peut être atteint au travers de l'indice -1

```python2
a[-1,:]= arange(3)
a
```

- les tableaux a une dimension existent en **Python** ainsi

```python2
a = zeros((4,3)); a[:,1].shape
```

```python2
a[:,1:2].shape 
```

- Attention **Python** utilise des références : 

```python2
a = zeros(4); b=a; b[1] = 10; a[1] # ne renvoie pas 0  mais 10!
```

- Pour copier un objet on utilise `.copy()` : ainsi

```python2
a = zeros(4); b=a.copy(); b[1] = 10; a[1] # Ok, renvoie 0
```

- **Python** autorise la *réplication* (broadcast en anglais) des matrices de dimensions différentes lors d'opérations 
terme à terme (on dit aussi  *au sens d'Hadamard*) :
si 2 matrices (ou tableaux N-D) ont une (ou plusieurs) dimensions différentes et que ces dimensions sont 
égales à 1 dans un des deux membres de l'opération alors cette matrice est étendue dans cette dimension jusqu'à ce qu'elle
égale l'autre, par exemple :

```python2
a = random.rand(10,3) # a est de dimension 10x3
m = mean(a,0) # m est de dimension 3
a_0 = a - m # OK, on peut verifier que linalg.norm(np.mean(a_0,0)) ~ 0
a.shape
```

mais si on change la taille de m

```python2
m = r_[m,1] # m est maintenant de taille 4
a_0 = a - m # error !
```

Une application bien pratique de la réplication est la construction d'une matrice à partir d'une fonction bilinéaire
appliquée à un vecteur et une forme linéaire. Ainsi pour générer la matrice $M_{ij} = x_i+y_j$, a partir des vecteurs
$x$ et $y$, on peut faire en **Python** 

```python2
x = arange(3)
y = x 
M = x[None,:] + y[:,None]
M
```

`None` permet de transformer des vecteurs 1D, en matrices en ajoutant une dimension égale à 1. On peut utiliser
ce genre d'astuce pour fabriquer une Vandermonde, au lieu d'utiliser les *listes  en comprehension*

```python2
x=array([1.,-2,3.14])
array([[x[i]**j for i in range(3)] for j in range(3)])
```

on fait une réplication avec l'opérateur d'exponentiation

```python2
x**arange(3)[:,None]
```

### Structures de contrôle
Pour coder vos algorithmes, vous avez les structures de contrôle de base :
- le branchement

# ```python
if x > 0:
    print 'positif'
elif x == 0:
    print 'zero'
else:
    print 'negatif'
# ```

- l'itération - appelée aussi boucle -

```python2
for i in range(3): # sur des entiers
     print i
```

```python2
for z in ["coucou", "hoho"]: # sur une liste
    print z
```

```python2
i  = 1 
while i < 10: # boucle tant que
      i *= 2 
print i      
```

Notez bien la présence du `:` suivi d'une indentation pour définir les blocs des structures  


### Fonctions
Pour déclarer une fonction on utilise les mot-clefs `def` et `return`. Par exemple, pour déclarer la fonction $f : (x,y) \mapsto (x+y,x \cdot y)$ on peut faire

```python2
def f(x,y):
    return x+y, x-y
a, b = f(2,3) # a et b sont des entiers 
f(2,3) # est un couple
```

si le corps de la fonction est très simple on peut utiliser une fonction anonyme avec le mot-clef `lambda`

```python2
(lambda x,y: ((x+y)/2,(x-y)/2))(*f(3,2))
```

le `*` avant le couple permet «d'exploser» le couple afin de donner 2 arguments à la fonction


## Quelques recommandations de programmation
 
- Éviter le plus possible les boucles **penser vectoriel**
- Si vous écrivez dans une matrice en utilisant des indices, pensez à l'allouer avant `a = empty(1000); `
- Éventuellement dés-allouer la mémoire qui n'est plus utilisée `del`; **Python** a un ramasse-miettes intégré, mais on 
peut l'aider(😃)
- Écrire des tests (doctests,unittest ou nose). Dans ce TP on utilisera les premiers. Exemple pour vérifier le résultat d'une fonction



```python2
import doctest
def mafonction(x):
   """
   >>> mafonction(5)
   42
   """
   return 42
doctest.testmod()
```

⚠️  : la valeur de retour correct est donnée sous forme de caractères, il ne doit pas y avoir d'espace. Si
de plus on veut vérifier une valeur de retour en décimale le mieux est d'utiliser une fonction `norm`, par exemple

```python2
def main():
   """
   >>> a = arange(0.,1.1,1/10.)
   >>> b = diff(a)
   >>> norm(b-0.1*ones(10))< 1e-12
   True
   """

doctest.testmod() 
```

-  écrire de la doc (docstring) :  

```python2
def mafonction(x):
   """
   résout l'equation du tout en O(1)
   Keywords arguments 
   x -- une matrice n par n
   """
   return 42
```

```python2
?mafonction
```

-  une manière d'estimer si vous codez - *proprement* - selon les standards **Python**, et  de lancer le programme `pylint` sur votre code : il vous attribuera une note sur 10, souvenant négative au départ!

-  Attention aux tabulations dans votre éditeur : à bannir, a cause des problèmes d'indentation liés à la syntaxe **Python**; préférer quatre espaces à une tabulation


# Laplacien et équation de Poisson
On va tout d'abord étudier différentes manières de calculer un laplacien en différences finies
sur le carré $\Omega = [0,1]^2$. En réalité, on ne va travailler que sur l'intérieur $\stackrel{o}{\Omega}$.
On considère une fonction $u :  \Omega \rightarrow \mathbb{R}$, que l'on souhaite approcher par ses valeurs
$$
U_{i,j} = u(ih,jh) \mbox{ pour } i,j=0,\cdots,n+1
$$
avec $h=\frac{1}{n+1}$. On considérera - *abusivement* - $U$ comme une matrice ou un vecteur - noté $u$ - de
$\mathbb{R}^{(n+2)^2}$ ou de $\mathbb{R}^{n\times n}$ en considérant que $u$ est nulle sur les bords
$$
u(x,y) = 0  \qquad \forall (x,y) \in \overline{\Omega}
$$
ce qui se traduit par
$$
U_{0,i} = U_{n+1,i} = U_{i,0} = U_{i,n+1} = 0  \mbox{ pour } i=0,\cdots, n+1
$$
Si $U$ est sous forme matricielle, on pourra utiliser la fonction suivante pour mettre des valeurs nulles
sur les 4 bords de la matrice : 

```python2
def ecrase_bords(X):
    """
    put zeros on the boundary of X
    >>> X=rand(3,3); ecrase_bords(X); X[0,0]
    0.0
    """
    assert(len(X.shape)==2)
    X[:, -1] = 0.
    X[:, 0] = 0.
    X[0, :] = 0.
    X[-1, :] = 0.
```

Pour discrétiser le laplacien sur une grille régulière on va utiliser l'approximation suivante, 
basée sur un développement limité
\begin{eqnarray}
\Delta u(x,y) & \approx & \frac{u(x-h_x, y) + u(x+ h_x, y) - 2 u(x,y)}{h^2} \\
              & + & \frac{u(x, y-h_y) + u(x, y + h_y) - 2 u(x,y)}{h^2}, 
\end{eqnarray}


Graphiquement, cela correspondera à appliquer le pochoir(stencil en anglais)  suivant
<center>
   <table>
      <tr> 
          <td><img src="fig/stencil.svg", width="200"> </td><td> <img src="fig/buffer.svg", width="400"> </td>
       </tr>    
   </table>    
</center>    
à l'intérieur d'une matrice avec les coefficients $d = -\frac{4}{h^2}$ , $w_x = w_y = \frac{1}{h^2}$
En première approche, on peut calculer $\Delta u$ comme une matrice dense d'ordre $n^2$. L'ordre
choisi pour déplier une matrice $n \times n$ en un vecteur de $\mathbb{R}^{n^2}$ est celui du C, i.e en ligne. C'est celui utilisé par défaut, lorsque vous appeler `.flatten()` sur une matrice **Numpy**.
Si on considère la matrice tridiagonale  
$$
K_n = \begin{pmatrix}
	2       & -1      & {\bf 0} \\
       -1      &  \ddots & -1 \\
       {\bf 0} & -1      & 2 \\
       \end{pmatrix},
$$

alors le laplacien 2D peut se construire sous de la façon suivante $\Delta u = -\frac{1}{h^2} K2D_n$ avec

$$
K2D_n =  K_n \otimes I_n + I_{n} \otimes  K_n.
$$

ou $\otimes$ est le [produit de kronecker](https://fr.wikipedia.org/wiki/Produit_de_Kronecker) de deux matrices 


## Questions

- faire une fonction `K1D` qui prend un entier $n$ en argument et construit la matrice $K_n$, on pourra utiliser `diag`
- ajouter les doctests suivants sur `K1D`
# ```python
"""
>>> norm(diag(K1D(4))-2.*ones(4)) < 1e-12
True
>>> norm(diag(K1D(3),-1)+ones(2)) < 1e-12
True
>>> norm(diag(K1D(4),1)+ones(3)) < 1e-12
True
"""
# ```

et rajouter à la fin de votre script **Python**,
# ```python
import doctest
doctest.testmod() 
# ```
pour exécuter les doctests à chaque fois que vous lancerez `run mon_module.py`

- coder les fonctions `v1D(n)` et `val1D(n)` qui calculent respectivement
$$
V^n_{k,l} =  \sin \left( \frac{kl\pi}{n+1} \right) \forall (k,l) \in \{1,\cdots,n\}^2 \mbox{ et }  \lambda^n_k =  2 - 2 \cos \left( \frac{k\pi}{n+1} \right)
$$
`v1D(n)` renverra une matrice et  `val1D(n)` un vecteur 
-  vérifier que  $\|K(n) V_1^n -\lambda_1^n  * V_1^n\| < \varepsilon$ en rajoutant les doctests suivants
# ```python
"""
>>> norm(val1D(3)[0]*v1D(3)[:,0]-dot(K1D(3),v1D(3)[:,0]))<1e-10
True
"""
# ```
pour `val1D` et 
# ```python
"""
>>> n=3
>>> V=v1D(n)
>>> aI=dot(V,V.T)
>>> a=aI[0,0]
>>> norm(aI-a*eye(n))<1e-12
True
"""
# ```
-  coder une fonction `K2D` qui construit $K2D(n)$; penser à utiliser `kron`


## Version creuse *a la mano*


Au lieu de considérer $\Delta u$ comme une matrice dense, on peut utiliser soit le format creux `scipy.sparse` des matrices, soit implanter l'opérateur linéaire $\Delta : u \mapsto \Delta u)$, directement comme une fonction. Dans
un premier temps nous allons code la seconde méthode. La première méthode est expliquée dans la section de bonus

L'idée est de construire une fonction - appelons la temporairement $f$ - qui réalise le produit  «matrice-vecteur»
\begin{eqnarray}
    f : \mathbb{R}^{n^2} &\rightarrow & \mathbb{R}^{n^2} \\
              x & \mapsto & K2D(n)x.
\end{eqnarray} 
avec $x = {\mathrm vec}(X)$ que l'on peut calculer grâce à `x = X[1:-1,1:-1].flatten()`,
en admettant que $X$ soit de dimension $n+2$ et contienne des zéros sur le bord.
Cela revient en définitive à calculer une matrice $Y$ de même taille que $X$ telle que
$$
Y_{i,j} = 4 X_{i,j} - X_{i-1,j} - X_{i+1,j} -X_{i,j-1} - X_{i,j+1} \mbox{ pour } i,j=2,\cdots,n+1
$$

Pour résoudre un tel système, on fera typiquement appel à une méthode itérative (CG, GMRES, etc..),
car le propre de ces méthodes est de ne pas construire explicitement la matrice de l'opérateur
mais juste de l'évaluer contre des vecteurs choisis au cours des itérations. Par rapport,
aux constructions précédentes, on va considérer que l'objet d'entrée de notre fonction est 
une matrice carrée d'ordre $n+2$ et qu'elle renverra une matrice de même dimension, dont 
les *indices internes* contiennent le laplacien discret 2D de la matrice d'entrée.
    
### Questions
       
- coder une fonction K2D_sparse qui prend en entrée une matrice $X$ d'ordre $n+2$ et 
renvoie une matrice du même ordre dont les indices internes ($\{1,\cdots,n+1\}$) contiennent 
$K2D(x)$. On s'efforcera de le faire sans boucle, pour respecter l'esprit vectoriel de **Numpy** 

- Vérifier que l'on trouve le même résultat qu'avec `K2D`
# ```python
>>> N = 5
>>> u=linspace(0, 1, N)
>>> X_in=sqrt(u[None,:])*u[:,None]**3; 
>>> X_in[0, :] = 0; X_in[-1, :] = 0; 
>>> X_in[:, 0] = 0; X_in[:, -1] = 0;
>>> norm(K2D_sparse(X_in)[1:-1,1:-1].flatten() - dot(K2D(N-2),X_in[1:-1,1:-1].flatten()))<1e-10
True
# ```
 


## Explication mathématique
Le but est de résoudre l'équation suivante,
$$
\begin{cases}
\frac{\partial u}{\partial t} - c \Delta u = 0 \\
u(x,t) = 1 \, \qquad \forall  t \in [0,T] \qquad \,  \forall x \in \partial \Omega
\end{cases}
$$
sur le domaine $\Omega = [0,1]^2$. On fixe $c = 1$, dans la suite.
On discrétise alors la dérivée en temps par un schéma d'ordre un (Euler explicite):
$$
\frac{\partial u}{\partial t}(x,t_k) \approx \frac{u(x, t_k + dt) - u(x, t_k)}{dt}
$$
et le laplacien ($\Delta u$) est discrétisé comme dans la section précédente. On arrive donc au schéma suivant, 
\begin{equation}
    u^{t_k +dt } = u^{t_k} - \frac{dt}{h^2} K2D u^n. 
\end{equation}

Pour assurer une *stabilité* au schéma précédent, on prendra $dt \leqslant \frac{h^2}{4}$.

### Questions
- écrire une fonction `init_param` prend en argument $n$ et qui renvoie le triplet $(h,dt,u)$ avec
 - $h = 1 / (n-1)$
 - $dt = \frac{h^2}{4}$
  - une matrice $u$ carrée d'ordre $n$, remplie de zéros avec des $1$ sur les bords
-  en utilisant `K2D_sparse`, écrire une fonction `chaleur` qui prend $h, dt, U^t$ en entrée et qui renvoie le couple $(U^{t+1}, e)$ avec
$$
e = \sum_{i,j=1}^n (u^{n+1}_{ij} - u^{n}_{ij})^2
$$
-  programmer la boucle en temps qui permet de calculer la suite de solutions. On veillera à itérer la boucle jusqu'à atteindre un nombre maximum d'itérations ou à atteindre une précision donnée pour l'erreur 
-  optionnel :  en utilisant le module `optparse`, ajouter à la ligne de commande les options concernant $n$, le nombre maximum d'itérations et la précision souhaitée
-  en utilisant `plot_wireframe, savefig` dans la boucle en temps , on pourra créer un film de la façon suivante
# ```python
from pylab import *
from mpl_toolkits.mplot3d import Axes3D
fig = figure()
ax = Axes3D(fig)
xx,yy = mgrid[0.:1.:n*1.j,0.:1.:n*1.j]
ax.plot_wireframe(xx, yy, u)
#boucle temporelle
for k in range(it_max):
      u, err = ...
      ax.clear()
      ax.plot_wireframe(xx, yy, u)
      ax.set_zlim3d(0.,1.)
      savefig('%05d.png' % k)
      ...
comm="ffmpeg -i %05d.png heat.avi; rm *.png"
import os
os.system(comm)
# ```


# Bonus

## Méthode spectrale

La structure du laplacien 2D est extrêmement spéciale, et autorise une accélération des calculs à l'aide de
la transformée de Fourier discrète (DFT), que tout numéricien digne de ce nom se doit de connaître. On peut
montrer que la transformée en Sinus discrète (DST) diagonalise le laplacien 2D, et que 
la résolution du système associé 
\begin{equation}
K2D_n X = F
\end{equation}     
est de complexité bien moindre. On ne détaille pas ici 
les mathématiques sous-jacentes, mais on recommande au lecteur intéressé de se référer,  par exemple  ou \cite{Peyre}.

En simplifiant, on peut écrire que 
$$
K2D_n = {\mathcal S_n}^{-1} \Lambda {\mathcal S_n}
$$
avec
$$ 
\Lambda_{i,j} = \lambda_i + \lambda_j \mbox{ et }  S_{k,l} =  \sin \left(\frac{kj\pi}{n+1}\right) 
\sin \left(\frac{lj\pi}{n+1}\right) 
$$
ce qui est à mettre en relation avec la définition tensorielle de $K2D$.
La matrice de passage ${\mathcal S}$ correspond à la DST, que l'on peut implanter à l'aide d'une 
FFT bidimensionnelle sous la forme suivante
$$
{\mathcal S(X)} = -\frac{1}{2(n+1)} \Re \left( FFT(\tilde{X}) \right)_{2:n+1,2:n+1}
$$ 
où 
$$
\tilde{X}=
 \begin{pmatrix}
     0         & {\bf 0}_{1,n}      & 0       &  {\bf 0}_{1,n} \\
 {\bf 0}_{n,1} & X            & {\bf 0}_{n,1} &  -X_{:,n:-1:1} \\ 
 0             & {\bf 0}_{1,n}      & 0       &  {\bf 0}_{1,n} \\
 {\bf 0}_{n,1} & -X_{n:-1:1,:} & {\bf 0}_{n,1} &  X_{n:-1:1,n:-1:1} \\
 \end{pmatrix}
$$

La résolution du système linéaire  devient alors
-  calculer les composantes de $F$ dans la base de diagonalisation : $\alpha = {\mathcal S}_n(F)$
-  diviser celles-ci par les valeurs propres : $\beta = \alpha / \lambda$
-  ré-appliquer la DST : $X = {\mathcal S}_n(\beta)$ 

### Questions
- coder une fonction `dst2D_fast`, qui calcule la DST d'une matrice $X$. Pour être sur du résultat, on comparera le résultat avec la fonction suivante :

# ```python
def dst1D(x):
    return real(-fft(r_[0, x, 0, -x[::-1]])[1:x.shape[0] + 1] / 2.j)

def dst2D_check(X):
    """
    calcule la transformee en sinus discrete, version boucle
    """
    Y = zeros(X.shape)
    for i in range(X.shape[0]):
        Y[i, :] = dst1D(X[i, :])
    for j in range(X.shape[1]):
        Y[:, j] = dst1D(Y[:, j])
    return Y  *  2. / (X.shape[0] + 1)
# ```
Pour renverser un vecteur ou une matrice penser à utiliser l'indexation suivante `X[::-1,:]`
On pourra rajouter un doctest basé sur le fait que ${\mathcal S}$ est une *involution*
- implanter une fonction `K2D_solve` basée sur `dst2D_fast`, qui résout le système. Pour générer la matrice des valeurs propres, on pourra faire une réplication en rajoutant une dimension artificielle.
- rajouter un doctest en fabriquant une solution artificielle avec avec `K2D_sparse` ou `K2D`

## Version creuse avec scipy.sparse

Le sous-module scipy.sparse permet de manipuler des matrices creuses sous beaucoup de format
différents : COO, CSC - le format de *Matlab* -, CSR, BSR, DIA, DOK et LIL. Ces différents s'adaptent
selon les besoins. Ainsi des formats comme COO ou LIL, sont plus adaptés pour ajouter ou supprimer
des indices à des matrices, tandis que les format CSC, BSR ou DIA sont réservés aux calculs proprement
dits (produit matriciel, addition). Le gros inconvénient actuel de **Scipy**, est qu'il ne profite pas
des architectures multi-cœurs lors de produits matrices-vecteurs. Par contre, si vous résolvez des systèmes 
creux de manière directe, l'appel à **Umfpack** appelle un BLAS éventuellement multi-cœurs. 

Pour assembler notre matrice `K2D`, nous allons considérer le format COO qui permet facilement
de construire une matrice creuse en donnant ses indices sous la forme de trois vecteurs `data, col` et `row` tels
que pour tout entrée  `A[i,j]` non nulle, il existe `k` tel que  `A[row[k],col[k]] = data[k]` .
Une méthode pour construire `K2D` est d'utiliser la définition tensorielle précédente
$$
K2D(n) =  K(n) \otimes I_n + I_{n} \otimes  K(n).
$$
en réfléchissant sur la structure de `K2D`, on déduit la construction suivante
$$
K2D(n) = \begin{pmatrix}
     	     B_n       & U_n      & {\bf 0} \\
              U_n      &  \ddots & U_n \\
              {\bf 0} & U_n      & B_n \\
              \end{pmatrix},
$$
avec les blocs
$$
B_n = K(n) + 2 I_n \mbox{ et } U_n = - I_n.
$$

- en utilisant le format `coo_matrix` et le codage `(data,ij)`, écrire une  fonction `K1D_coo` qui construit la matrice creuse $K(n)$ et la tester avec le doctest suivant

# ```python
>>> norm(K1D_coo(3).todense()-K1D(3))
0.0
# ```
- écrire une fonction `K2D_coo` qui construit la matrice creuse $K2D(n)$. Pour s'aider on pourra utiliser judicieusement la fonction `tile` qui permet de dupliquer des matrices. Là encore, on pourra tester le résultat avec

# ```python
>>> norm(K2D_coo(6).todense()-K2D(6))
0.0
# ```
- «espionner», le résultat de `K1D_coo` et `K2D_coo` avec la fonction `spy`

 ## Comparaison des performances

Maintenant qu'on a programmé ces quatre algorithmes de résolution, on va évaluer leur performance. Pour cela, on vous fournit une fonction `chrono` dans le fichier `mesure.py`. Vous pouvez l'utiliser avec `from mesure import chrono`.

- en vous inspirant du doctest de la version creuse *a la mano*, écrire une fonction `create_instance` qui construit un problème de taille $N$, sous la forme d'une matrice $X$ et d'un second membre $F$ tel que $K2D x = F$. on utilisera le couple $(X,F)$ dans les questions suivantes
-  grâce au module `optparse`, rajouter une option sur la ligne de commande qui permet de récupérer $N$.
-  en utilisant `solve` dans `numpy.linalg`, résoudre le système avec une matrice dense : on affichera  l'erreur commise, ainsi que le temps d'exécution
-  faire le même travail avec la version creuse (coo) : on utilisera `spsolve` du sous module `scipy.sparse.linalg`
-  idem avec la version creuse  *à la mano* : pour inverser le système, on créera un opérateur linéaire `LinearOperator` en écrivant au préalable une fonction `prod_mat_vect` qui applique  `K2D_sparse` à une matrice construite à partir d'un vecteur de taille $N * N$. On pourra ensuite appliquer la fonction  `cg`. Les deux fonctions nécessaires sont dans le sous-module `scipy.sparse.linalg`
-  rajouter un test de performance pour la version par fft. 
-  faites tourner votre programme avec N = 10, 100, 500, 1000. On pourra mettre une condition suivant $N$ sur la version dense pour ne pas l'exécuter quand $N > 100$. On pourra mettre une bascule sur l'emploi de `dst2D_fast` ou ` dst2D_check` pour $N> 500$. A titre d'information, voila un test pour $N=100$

# ```python
tpsconv = 7.884979e-03
sparse (coo) err = 1.674060e-14, tps = 7.649112e-02
sparse (mano) err = 3.832052e-14, tps = 2.963423e-01
fft err = 1.114123e-12, tps = 8.898497e-03 
# ```
On voit que la version utilisant la `DFT` est clairement plus rapide.


## Appel d'une routine fortran
Il y a plusieurs mécanismes pour appeler du code Fortran ou C dans Python : F2py, SWIG, Cython ou ctypes: c'est cette dernière option que nous allons choisir
Supposons que l'on recodé la fonction `K2D_sparse` en fortran dans le fichier k2d.f90
# ```fortran
module heat
    use iso_c_binding, only: c_int32_t, c_double
    public :: stencil
contains
    subroutine stencil(m, n, u_in,  u_out, error) bind(C, name="stencil_4")

        implicit none
        integer(c_int32_t), intent(in) :: m, n
        real(c_double), dimension( 1:m, 1:n ), intent(in) :: u_in
        real(c_double), dimension( 1:m, 1:n ), intent(out) :: u_out
        real(c_double), intent(out) :: error

        u_out(2:m-1,2:n-1) = 4.d0 * u_in(2:m-1, 2:n-1) &
                                  - u_in(1:m-2, 2:n-1) &
                                  - u_in(3:m, 2:n-1)   &
                                  - u_in(2:m-1,1:n-2)  &
                                  - u_in(2:m-1,3:n)
        error  =  sum((u_out(:,:) - u_in(:,:))**2)

    end subroutine stencil
end module heat
# ```
On pourra compiler ce code avec gfortran en faisant

# ```sh
gfortran -o libk2d.so --shared k2d.f90
# ```

En utilisant le module **Python** `ctypes` on peut appeler la routine avec le code suivant

# ```python
import ctypes
from ctypes import CDLL, POINTER, c_int32, c_double, byref
from numpy import zeros, empty
fortran=CDLL("./libk2d.so")
m = n = 8
a=zeros((m,n), dtype="double")
a[[0,-1],:]=1.
a[:,[0,-1]]=1.
b=zeros((m,n), dtype="double")
err = c_double(0.)
m , n = map(c_int32, a. shape)
fortran.stencil_4(byref(m),  byref(n), a.ctypes.data_as(POINTER(c_double)), b.ctypes.data_as(POINTER(c_double)), byref(err))
# ```

Attention, comme Fortran passes les arguments de fonction par adresse, on est obligé d'utiliser la fonction byref
pour des arguments non-modifiés dans la fonction

### Bibliographie
- Peyré, G.,  *L'algèbre discrète de la transformée de Fourier *
- Strang, G., *Computational Science and Engineering*, Wellesley-Cambridge Press  
